import { request } from '@penta-b/ma-lib';



export const callQueryService = (data, endpoint, action , errorNotification) => {
  request.post(
    endpoint,
    data
  ).then((response) => {
    action(response)
  }).catch(error => {
    console.log(error)
    if(errorNotification){
      errorNotification(error)
    }
  })
}