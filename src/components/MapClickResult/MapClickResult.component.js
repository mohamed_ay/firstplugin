import React from "react";
import { connect } from "react-redux";
import { withLocalize, selectorsRegistry } from "@penta-b/ma-lib";
import { components } from "@penta-b/grid";
import { callQueryService } from "../query";
import { getLocale } from "@penta-b/ma-lib";
const Grid = components.Grid;
class MapClickResult extends React.Component {
  constructor(props) {
    super(props);
    this.state = { data: [], showGrid: false, selectedLayer: null };
   
  }
  componentDidMount() {
    const { settings } = this.props;
    if (settings && settings.layers) {
      this.setState({
        selectedLayer: settings.layers[0],
      });
    }
  }

  render() {
    const { coordinates, t, settings } = this.props;
    var parsedData=[]
   var configlay=[]
   var features=[]
   let grid;
    console.log("hi", settings);
    var data = [];
    console.log(this.props);
    const QueryCoordinates = () => {
      //Data to be sent to the query service

      var dataSource = null;
      var returns = [];
      var geoField = null;

      dataSource = this.props.settings.layers[0].datasource;
      dataSource["layerName"] = this.props.settings.layers[0].layerName;

      geoField = this.props.settings.layers[0].geometryField.fieldName;
      returns = this.props.settings.layers[0].fields;
      returns = [
        ...this.props.settings.layers[0].fields,
        this.props.settings.layers[0].geometryField,
      ];
      this.layerId = this.props.settings.layers[0].id;
      this.keyField = this.props.settings.layers[0].keyField;

      data.push({
        dataSource: dataSource,
        filter: {
          logicalOperation: "AND",
          conditionList: [
            {
              tabularCondition: null,
              spatialCondition: {
                key: geoField,
                geometry: JSON.stringify(coordinates),
                spatialRelation: "INTERSECT",
              },
              filter: null,
            },
          ],
        },
        returns: returns,
        crs: this.props.map.projection.code,
      });
      console.log(returns);
      console.log(data);
      callQueryService(data, "/query/api/query/queryFeature", (res) => {
        console.log("RES", res);
         features = JSON.parse(res.data[0].features).features;
        console.log("ggg", features);
        createGrid(features)
        this.setState({
          showGrid: true,
        });
      });
    };
    const createGrid = (features) => {
      console.log("fff",features)
      let allFeatures = modifyFeatures(features);
      console.log("all",allFeatures)
      for (let i = 0; i < allFeatures.length; i++) {
        parsedData.push({ ...allFeatures[i].properties });
      }
      console.log("parsed",parsedData)
      var columnss = this.props.settings.layers[0].fields;
      configlay = columnss.map((field) => {
        return {
          id: field.id,
          name: field.fieldName,
          type: field.dataType,
          display: field.display,
          sortable: true,
          filterable: false
         
        };
      });
      console.log("col",configlay)
    };
    
    const modifyFeatures=(features)=> {
      let newFeatures = [];
      console.log("jjjj",features)
      features &&
        features.forEach((feature) => {
          
          // get features returned data/properties
          let properties = {
            ...feature.properties,
            
            // these are the extra data for grid buttons
            // id: layerSettings.id,
            // layerName: layerSettings.layerName,
            // alias: layerSettings.alias,
            // idField: layerSettings.keyField,
            // fields: layerSettings.fields.map((f) => ({
            //   ...f,
            //   value: feature.properties[f.fieldName]
            //     ? feature.properties[f.fieldName]
            //     : null,
            // })),
            // geometry: feature.geometry ? feature.geometry : null,
          };
          
          newFeatures.push({ ...feature, properties });
          console.log("features",newFeatures)
        });
        
      return newFeatures;
    }
    ;
    grid = (
      <div>
        <Grid
          settings={{
       
            language: getLocale() && getLocale().name,
            maxPages: 1,
            data:parsedData,
            columns:configlay,
            selectable: false,
            sortable: true,
            filterable: true,
            resizable: false,
            manual: false,
          }}
        ></Grid>
      </div>
    );
    return (
      <div>
        <button onClick={QueryCoordinates}>Query</button>
        <div>{grid}</div>
      </div>
    );
  }
}
const mapStateToProps = (state, ownProps) => {
  return {
    map: selectorsRegistry.getSelector(
      "selectMapReducers",
      state,
      ownProps.reducerId
    ),
  };
};

export default connect(mapStateToProps)(
  withLocalize(MapClickResult, "map-click-namespace")
);
